package com.sarawut.bookstore

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.databinding.FragmentBookDetailBinding
import com.sarawut.bookstore.viewmodels.BookItemViewModel
import com.sarawut.bookstore.viewmodels.BookItemViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

class BookDetailFragment : Fragment() {
    private var _binding: FragmentBookDetailBinding? = null
    private val binding get() = _binding!!

    private var bookId: Int = 0
    private lateinit var currentBook: Book
    private var amount_picked: Int = 1

    private val viewModel: BookItemViewModel by activityViewModels {
        BookItemViewModelFactory(
            (activity?.application as BookListApplication).database.bookDao()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            bookId = it.getInt(BOOK_ID).toInt()
        }
        runBlocking(Dispatchers.IO) {
            currentBook = viewModel.getBook(bookId).first()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBookDetailBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar!!.title = currentBook.title
        setDetail(currentBook)

        binding.detailAddToCart.setOnClickListener {
            viewModel.addBookToCart(bookId, amount_picked)
            val action = BookDetailFragmentDirections.actionBookDetailFragmentToMainMenuFragment()
            view.findNavController().navigate(action)
        }

        binding.detailSeekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (progress == null) {
                    binding.detailAmountToCart.text = "Amount : -"
                } else {
                    binding.detailAmountToCart.text = "Amount : " + progress.toString()
                    updatePrice()
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                if (p0 != null) {
                    amount_picked = p0.progress
                    binding.detailAddToCart.isEnabled = amount_picked != 0
                    updatePrice()

                } else {
                    amount_picked = 0
                    updatePrice()
                }
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                if (p0 != null) {
                    amount_picked = p0.progress
                    binding.detailAddToCart.isEnabled = amount_picked != 0
                    updatePrice()

                } else {
                    amount_picked = 0
                    updatePrice()
                }
            }
        })
    }

    fun updatePrice() {
        val prices = currentBook.price * amount_picked
        if (prices > 0) {
            binding.detailPriceToCart.text = prices.toString()
                .substring(0, prices.toString().length - 2) + "." +
                    prices.toString()
                        .substring(prices.toString().length - 2) + " THB"
        } else {
            binding.detailPriceToCart.text = "0.00 THB"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setDetail(currentBook: Book) {
        binding.detailSeekbar.isEnabled = true
        binding.detailAddToCart.isEnabled = true
        binding.detailPriceToCart.visibility = TextView.VISIBLE

        val context: Context = binding.bookImage.context
        val id: Int = context.resources
            .getIdentifier(currentBook.imageSrc, "drawable", context.getPackageName())
        binding.bookImage.setImageResource(id)

        binding.detailName.text = currentBook.title
        binding.detailWriter.text = currentBook.writer
        binding.detailCategory.text = currentBook.category
        binding.detailYear.text = currentBook.year.toString()
        binding.detailPublisher.text = currentBook.publisher
        binding.detailAdded.text = currentBook.dateAdded
        binding.detailPrice.text = currentBook.price.toString().substring(
            0,
            currentBook.price.toString().length - 2
        ) + "." + currentBook.price.toString()
            .substring(currentBook.price.toString().length - 2) + " THB"
        binding.detailStock.text = currentBook.stock.toString()

        if (viewModel.findCartId(bookId)) {
            val stockAddMax = currentBook.stock - viewModel.getAmount(bookId)
            if (stockAddMax > 0) {
                binding.detailSeekbar.max = stockAddMax
            } else {
                if (currentBook.stock > 0) {
                    binding.detailAmountToCart.text = "Available products is in your cart."
                } else {
                    binding.detailAmountToCart.text = "Out of Stock."
                }
                binding.detailSeekbar.isEnabled = false
                binding.detailAddToCart.isEnabled = false
                binding.detailPriceToCart.visibility = TextView.INVISIBLE
            }
        } else {
            binding.detailSeekbar.max = currentBook.stock
        }

        updatePrice()

    }

    companion object {
        var BOOK_ID = "bookID"
    }
}