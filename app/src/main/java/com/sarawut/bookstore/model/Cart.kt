package com.sarawut.bookstore.model

data class Cart(val bookId: Int, var amount: Int)
