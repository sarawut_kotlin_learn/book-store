package com.sarawut.bookstore

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.bookstore.adapter.BookGridAdapter
import com.sarawut.bookstore.adapter.BookLinearAdapter
import com.sarawut.bookstore.databinding.FragmentBookDetailBinding
import com.sarawut.bookstore.databinding.FragmentCategoryBinding
import com.sarawut.bookstore.viewmodels.BookItemViewModel
import com.sarawut.bookstore.viewmodels.BookItemViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class CategoryFragment : Fragment() {
    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!

    private var categoryType: String = ""

    private val viewModel: BookItemViewModel by activityViewModels {
        BookItemViewModelFactory(
            (activity?.application as BookListApplication).database.bookDao()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var categoryId: Int = 0
        arguments?.let {
            categoryId = it.getInt(CATEGORY_ID).toInt()
        }

        runBlocking(Dispatchers.IO) {
            categoryType = viewModel.categoryTypeString(categoryId).first()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCategoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar!!.title = categoryType
        categoryAllRecyclerView(view)
        categoryRecentRecyclerView(view)
    }

    private fun categoryAllRecyclerView(view: View) {
        val recyclerView: RecyclerView = binding.scrollingAllBook
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext())

        val bookLinearAdapter = BookLinearAdapter {
            val action = CategoryFragmentDirections.actionCategoryFragmentToBookDetailFragment(
                bookID = it.id
            )
            view.findNavController().navigate(action)
        }

        recyclerView.adapter = bookLinearAdapter

        lifecycle.coroutineScope.launch {
            viewModel.categoryBook(categoryType).collect() {
                bookLinearAdapter.submitList(it)
            }
        }
    }

    private fun categoryRecentRecyclerView(view: View) {
        val recyclerView: RecyclerView = binding.scrollingRecentBook
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        val bookItemAdapter = BookGridAdapter {
            val action = CategoryFragmentDirections.actionCategoryFragmentToBookDetailFragment(
                bookID = it.id
            )
            view.findNavController().navigate(action)
        }

        recyclerView.adapter = bookItemAdapter
        lifecycle.coroutineScope.launch {
            viewModel.mostCategoryBook(categoryType).collect() {
                bookItemAdapter.submitList(it)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        var CATEGORY_ID = "categoryID"
    }
}