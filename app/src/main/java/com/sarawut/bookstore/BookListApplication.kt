package com.sarawut.bookstore

import android.app.Application
import com.sarawut.bookstore.database.ItemBookDatabase

class BookListApplication : Application() {
    val database: ItemBookDatabase by lazy {
        ItemBookDatabase.getDatabase(this)
    }
}