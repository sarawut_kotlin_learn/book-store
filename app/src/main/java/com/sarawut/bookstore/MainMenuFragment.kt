package com.sarawut.bookstore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.bookstore.adapter.BookGridAdapter
import com.sarawut.bookstore.adapter.BookLinearAdapter
import com.sarawut.bookstore.adapter.CategoryGridAdapter
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.databinding.FragmentMainMenuBinding
import com.sarawut.bookstore.model.Cart
import com.sarawut.bookstore.viewmodels.BookItemViewModel
import com.sarawut.bookstore.viewmodels.BookItemViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainMenuFragment : Fragment() {
    private var _binding: FragmentMainMenuBinding? = null
    private val binding get() = _binding!!

    private val viewModel: BookItemViewModel by activityViewModels {
        BookItemViewModelFactory(
            (activity?.application as BookListApplication).database.bookDao()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainMenuBinding.inflate(inflater, container, false)
        (activity as AppCompatActivity).supportActionBar!!.title = "Mainmenu"

        if (viewModel.allCartList.size <= 0) {
            binding.cartFrame.visibility = View.GONE
        } else {
            binding.cartFrame.visibility = View.VISIBLE
        }

        var amountCart = 0
        var price = 0
        for (i: Cart in viewModel.allCartList) {
            var currentBook: Book
            runBlocking(Dispatchers.IO) {
                currentBook = viewModel.getBook(i.bookId).first()
            }

            amountCart += i.amount
            price += (i.amount * (currentBook.price / 100))
        }
        binding.cartButtonAmount.text = amountCart.toString() + " items"
        binding.cartButtonPrice.text = price.toString() + " THB"

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainCategoryRecyclerView(view)
        mainRecentRecyclerView(view)
        mainAllRecyclerView(view)

        binding.cartFrame.setOnClickListener {
            val action = MainMenuFragmentDirections.actionMainMenuFragmentToCartFragment()
            view.findNavController().navigate(action)
        }
    }

    private fun mainCategoryRecyclerView(view: View) {
        val recyclerView: RecyclerView = binding.scrollingCategory
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        val categoryGridAdapter = CategoryGridAdapter {
            val action = MainMenuFragmentDirections.actionMainMenuFragmentToCategoryFragment(
                categoryID = it.id
            )
            view.findNavController().navigate(action)
        }

        recyclerView.adapter = categoryGridAdapter

        lifecycle.coroutineScope.launch {
            viewModel.categoryTypeList().collect() {
                categoryGridAdapter.submitList(it)
            }
        }
    }

    private fun mainAllRecyclerView(view: View) {
        val recyclerView: RecyclerView = binding.scrollingAllBook
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext())

        val bookLinearAdapter = BookLinearAdapter {
            val action = MainMenuFragmentDirections.actionMainMenuFragmentToBookDetailFragment(
                bookID = it.id
            )
            view.findNavController().navigate(action)
        }

        recyclerView.adapter = bookLinearAdapter

        lifecycle.coroutineScope.launch {
            viewModel.allBook().collect() {
                bookLinearAdapter.submitList(it)
            }
        }
    }

    private fun mainRecentRecyclerView(view: View) {
        val recyclerView: RecyclerView = binding.scrollingRecentBook
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        val bookItemAdapter = BookGridAdapter {
            val action = MainMenuFragmentDirections.actionMainMenuFragmentToBookDetailFragment(
                bookID = it.id
            )
            view.findNavController().navigate(action)
        }

        recyclerView.adapter = bookItemAdapter
        lifecycle.coroutineScope.launch {
            viewModel.mostBook().collect() {
                bookItemAdapter.submitList(it)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
