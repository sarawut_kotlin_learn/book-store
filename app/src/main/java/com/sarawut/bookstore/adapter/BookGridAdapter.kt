package com.sarawut.bookstore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.databinding.BookItemBinding

class BookGridAdapter(private val onItemClicked: (Book) -> Unit) :
    ListAdapter<Book, BookGridAdapter.BookItemViewHolder>(DiffCallback) {
    class BookItemViewHolder(private var binding: BookItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(book: Book) {
            val context: Context = binding.bookImageSample.context
            val id: Int = context.resources
                .getIdentifier(book.imageSrc, "drawable", context.getPackageName())
            binding.bookImageSample.setImageResource(id)

            binding.bookTitleSample.text = book.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookItemViewHolder {
        val viewHolder = BookItemViewHolder(
            BookItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: BookItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Book>() {
            override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
                return oldItem == newItem
            }
        }
    }


}