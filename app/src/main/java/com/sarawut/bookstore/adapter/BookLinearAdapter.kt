package com.sarawut.bookstore.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.databinding.BookHorizonItemBinding

class BookLinearAdapter(private val onItemClicked: (Book) -> Unit) :
    ListAdapter<Book, BookLinearAdapter.BookItemHorizonViewHolder>(DiffCallback) {

    class BookItemHorizonViewHolder(private var binding: BookHorizonItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(book: Book) {
            val context: Context = binding.bookImageSampleHorizon.context
            val id: Int = context.resources
                .getIdentifier(book.imageSrc, "drawable", context.getPackageName())
            binding.bookImageSampleHorizon.setImageResource(id)

            binding.bookTitleSampleHorizon.text = book.title
            binding.bookCategorySampleHorizon.text = book.category
            binding.bookPriceSampleHorizon.text = book.price.toString()
                .substring(0, book.price.toString().length - 2) + "." + book.price.toString()
                .substring(book.price.toString().length - 2) + " THB"
            binding.bookYearSampleHorizon.text = "(" + book.year.toString() + ")"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookItemHorizonViewHolder {
        val viewHolder = BookItemHorizonViewHolder(
            BookHorizonItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: BookItemHorizonViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Book>() {
            override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
                return oldItem == newItem
            }
        }
    }


}