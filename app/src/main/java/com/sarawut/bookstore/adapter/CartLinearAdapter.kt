package com.sarawut.bookstore.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.bookstore.BookListApplication
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.databinding.BookCartItemBinding
import com.sarawut.bookstore.model.Cart
import com.sarawut.bookstore.viewmodels.BookItemViewModel
import com.sarawut.bookstore.viewmodels.BookItemViewModelFactory

class CartLinearAdapter(private val onItemClicked: (Pair<Book, Cart>) -> Unit) :
    ListAdapter<Pair<Book, Cart>, CartLinearAdapter.CartItemHorizonViewHolder>(DiffCallback) {

    class CartItemHorizonViewHolder(private var binding: BookCartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(book: Pair<Book, Cart>) {
            val context: Context = binding.bookImageCartHorizon.context
            val id: Int = context.resources
                .getIdentifier(book.first.imageSrc, "drawable", context.getPackageName())
            binding.bookImageCartHorizon.setImageResource(id)

            binding.bookTitleCartHorizon.text = book.first.title
            binding.bookCategoryCartHorizon.text = book.first.category

            val prices = book.second.amount * book.first.price
            if(prices > 0) {
                binding.bookPriceCartHorizon.text = prices.toString()
                    .substring(0, prices.toString().length - 2) + "." +
                        prices.toString()
                            .substring(prices.toString().length - 2) + " THB"
            }else {
                binding.bookPriceCartHorizon.text = "0.00 THB"
            }


            binding.bookWriteCartHorizon.text = book.first.writer
            binding.bookYearCartHorizon.text = "(" + book.first.year.toString() + ")"
            binding.bookAmountCartHorizon.text = "( " + book.second.amount.toString() + " )"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartItemHorizonViewHolder {
        val viewHolder = CartItemHorizonViewHolder(
            BookCartItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: CartItemHorizonViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Pair<Book, Cart>>() {
            override fun areItemsTheSame(
                oldItem: Pair<Book, Cart>,
                newItem: Pair<Book, Cart>
            ): Boolean {
                return oldItem.first.id == newItem.first.id
            }

            override fun areContentsTheSame(
                oldItem: Pair<Book, Cart>,
                newItem: Pair<Book, Cart>
            ): Boolean {
                return oldItem == newItem
            }
        }
    }


}