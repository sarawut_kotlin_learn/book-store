package com.sarawut.bookstore.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.database.book.BookDao
import com.sarawut.bookstore.database.book.Category
import com.sarawut.bookstore.model.Cart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking

class BookItemViewModel(private val bookItem: BookDao) : ViewModel() {
    fun allBook(): Flow<List<Book>> = bookItem.getAll()
    fun mostBook(): Flow<List<Book>> = bookItem.getMost()
    fun mostCategoryBook(category: String): Flow<List<Book>> = bookItem.getCategoryMost(category)
    fun categoryBook(name: String): Flow<List<Book>> = bookItem.getCategoryAll(name)
    fun getBook(id: Int): Flow<Book> = bookItem.getBook(id)
    fun categoryTypeList(): Flow<List<Category>> = bookItem.getTypeCategory()
    fun categoryTypeString(id: Int): Flow<String> = bookItem.getTypeStringCategory(id)

    fun getMultiBook(idList: ArrayList<Int>): Flow<List<Book>> {
        var list: ArrayList<Book> = ArrayList()
        for (i in idList) {
            var currentBook: Book
            runBlocking(Dispatchers.IO) {
                currentBook = getBook(i).first()
            }
            list.add(currentBook)
        }
        val listFlow: Flow<List<Book>> = flowOf(list)
        Log.d("Hello", "send " + list)
        return listFlow
    }

    private var _cartList: ArrayList<Cart> = ArrayList<Cart>()
    private var _cartListS: ArrayList<Pair<Book, Cart>> = ArrayList()

    fun addBookToCart(bookID: Int, amount: Int) {
        var founded = false
        for (i: Cart in _cartList) {
            if (i.bookId == bookID) {
                for (j in 1..amount) {
                    addAmount(i.bookId)
                }
                founded = true
            }
        }
        if (!founded) {
            _cartList.add(Cart(bookID, amount))

            var currentBook: Book
            runBlocking(Dispatchers.IO) {
                currentBook = getBook(bookID).first()
            }
            _cartListS.add(Pair(currentBook, Cart(bookID, amount)))
        }
    }

    fun allCartListExt(): Flow<ArrayList<Pair<Book, Cart>>> {
        return flowOf(_cartListS)
    }

    val allCartList: ArrayList<Cart>
        get() = _cartList

    private val allCartSList: ArrayList<Pair<Book, Cart>>
        get() = _cartListS

    val cartList: Int
        get() = _cartList.size

    fun findCartId(id: Int): Boolean {
        for (i: Cart in _cartList) {
            if (id == i.bookId) {
                return true
            }
        }
        return false
    }

    fun idCartList(): ArrayList<Int> {
        var list: ArrayList<Int> = ArrayList()
        for (i: Cart in _cartList) {
            list.add(i.bookId)
        }
        return list
    }

    fun clearCart() {
        _cartList.clear()
        _cartListS.clear()
    }

    fun setAmount(id: Int, amounts: Int) {
        for (i in 0.._cartList.size-1) {
            if (_cartList.get(i).bookId == id) {
                if (amounts > 0) {
                    _cartList.get(i).amount = amounts
                    _cartListS.get(i).second.amount = amounts
                } else {
                    _cartList.removeAt(i)
                    _cartListS.removeAt(i)
                    break;
                }
            }
        }
    }

    fun addAmount(id: Int) {
        for (i: Cart in _cartList) {
            if (i.bookId == id) {
                i.amount++
            }
        }

        for (i: Pair<Book, Cart> in _cartListS) {
            if (i.second.bookId == id) {
                i.second.amount++
            }
        }
    }

    fun getAmount(id: Int): Int {
        var amount = 0
        for (i: Cart in _cartList) {
            if (i.bookId == id) {
                amount = i.amount
            }
        }
        return amount
    }

    fun getAmountCart(): Int {
        var amountCart = 0

        for (i: Cart in allCartList) {
            var currentBook: Book
            runBlocking(Dispatchers.IO) {
                currentBook = getBook(i.bookId).first()
            }

            amountCart += i.amount
        }

        return amountCart
    }

    fun getPriceCart(): Int {
        var price = 0
        for (i: Pair<Book, Cart> in allCartSList) {
            price += (i.first.price * i.second.amount)
        }
        return price
    }

}

class BookItemViewModelFactory(
    private val bookItem: BookDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BookItemViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BookItemViewModel(bookItem) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

