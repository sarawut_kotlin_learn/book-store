package com.sarawut.bookstore.database.book

import androidx.room.Query
import androidx.room.Dao
import kotlinx.coroutines.flow.Flow

@Dao
interface BookDao {

    @Query("SELECT * FROM book ORDER BY id ASC")
    fun getAll(): Flow<List<Book>>

    @Query("SELECT * FROM book ORDER BY \"id\" DESC LIMIT 5")
    fun getMost(): Flow<List<Book>>

    @Query("SELECT * FROM book WHERE category = :category ORDER BY \"id\" DESC LIMIT 5")
    fun getCategoryMost(category: String): Flow<List<Book>>

    @Query("SELECT * FROM book WHERE category = :category ORDER BY id ASC")
    fun getCategoryAll(category: String): Flow<List<Book>>

    @Query("SELECT * FROM Book WHERE id = :id")
    fun getBook(id: Int): Flow<Book>

    @Query("SELECT id, category FROM book GROUP BY category ORDER BY id ASC")
    fun getTypeCategory(): Flow<List<Category>>

    @Query("SELECT category FROM book WHERE id = :id")
    fun getTypeStringCategory(id: Int): Flow<String>
}