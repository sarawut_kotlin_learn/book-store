package com.sarawut.bookstore.database.book

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Book")
data class Book(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "imgsrc") val imageSrc: String,
    @ColumnInfo(name = "writer") val writer: String,
    @ColumnInfo(name = "category") val category: String,
    @ColumnInfo(name = "publisher") val publisher: String,
    @ColumnInfo(name = "year") val year: Int,
    @ColumnInfo(name = "date") val dateAdded: String,
    @ColumnInfo(name = "price") val price: Int,
    @ColumnInfo(name = "stock") val stock: Int,

    )
