package com.sarawut.bookstore.database.book

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Book")
data class Category(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "category") val category: String
)
