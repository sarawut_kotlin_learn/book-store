package com.sarawut.bookstore.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sarawut.bookstore.database.book.Book
import com.sarawut.bookstore.database.book.BookDao

@Database(entities = arrayOf(Book::class), version = 1)
abstract class ItemBookDatabase : RoomDatabase() {
    abstract fun bookDao(): BookDao

    companion object {
        @Volatile
        private var INSTANCE: ItemBookDatabase? = null

        fun getDatabase(context: Context): ItemBookDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    ItemBookDatabase::class.java,
                    "item_database"
                )
                    .createFromAsset("database/book_data.db")
                    .build()
                INSTANCE = instance

                instance
            }
        }
    }
}