package com.sarawut.bookstore

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.bookstore.adapter.BookLinearAdapter
import com.sarawut.bookstore.adapter.CartLinearAdapter
import com.sarawut.bookstore.databinding.BookCartItemBinding
import com.sarawut.bookstore.databinding.FragmentCartBinding
import com.sarawut.bookstore.viewmodels.BookItemViewModel
import com.sarawut.bookstore.viewmodels.BookItemViewModelFactory
import kotlinx.coroutines.launch

class CartFragment : Fragment() {
    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private val viewModel: BookItemViewModel by activityViewModels {
        BookItemViewModelFactory(
            (activity?.application as BookListApplication).database.bookDao()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar!!.title = "Cart"
        cartListRecyclerView(view)

        binding.ButtonCheckout.setOnClickListener {
            viewModel.clearCart()
            val action = CartFragmentDirections.actionCartFragmentToCheckout()
            view.findNavController().navigate(action)
        }

        binding.CartTopDeleteAll.setOnClickListener {
            val dialogClickListener =
                DialogInterface.OnClickListener { dialog, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            viewModel.clearCart()
                            val action =
                                CartFragmentDirections.actionCartFragmentToMainMenuFragment()
                            view.findNavController().navigate(action)

                            Toast.makeText(requireContext(), "Cart cleared!", Toast.LENGTH_SHORT)
                                .show()
                        }

                        DialogInterface.BUTTON_NEGATIVE -> {
                            dialog.dismiss()
                        }
                    }
                }

            val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())

            builder.setMessage("Do you want to clear all item in your cart?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show()


        }
    }

    private fun cartListRecyclerView(view: View) {
        val recyclerView: RecyclerView = binding.CartList
        recyclerView.layoutManager =
            LinearLayoutManager(requireContext())

        val cartLinearAdapter = CartLinearAdapter {
            val action = CartFragmentDirections.actionCartFragmentToCartDetailFragment(
                bookID = it.second.bookId
            )
            view.findNavController().navigate(action)
        }

        recyclerView.adapter = cartLinearAdapter

        lifecycle.coroutineScope.launch {
            viewModel.allCartListExt().collect() {

                cartLinearAdapter.submitList(it)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        binding.CartTopTitle.text = "Total " + viewModel.getAmountCart().toString() + " item"

        val prices = viewModel.getPriceCart()
        if(prices > 0) {
            binding.ButtonCheckoutPrice.text = prices.toString()
                .substring(0, prices.toString().length - 2) + "." +
                    prices.toString()
                        .substring(prices.toString().length - 2) + " THB"
        }else {
            binding.ButtonCheckoutPrice.text = "0.00 THB"
        }

        binding.ButtonCheckoutAmount.text = "( " + viewModel.getAmountCart().toString() + " )"
        return binding.root
    }
}